# Checklist

## Is it secure?

- CSP
- SSL/TLS
- Snyk
- HTTPS
- Test in Mozilla Observatory

## Is it accessible?

- Test in AXE or similar

## Is it performant?

- HTTP2
- Optimize
- Lighthouse
- Loads in sub-2 secs

## Is it tested?

- Tests for all major functionality
- Test with Jest
