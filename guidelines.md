# Code Guidelines

## Application requirements

- Application code is tested
- Application is secure
- Application is accessible
- Application is highly performant
- All components are cross-platform
- All design is automatically, or at least very tightly, synced into code
- All work is cross-examined (code review, design review, implementation review…)

## Linting and hygiene

- Use an extensible, well-supported IDE (Atom, VS Code, Brackets, Sublime…)
- Set up your IDE to use all the available linting
- Follow linting rules
- Our primary linting guidelines come from Airbnb
- Always fix your linting errors/warnings before pushing code

## Syntax

- Most style syntax comes from AirBnB’s configs – always respect linting messages even for styles
- Use SUITCSS syntax for CSS (also includes Styled Components: [see resource](https://github.com/suitcss/suit/blob/master/doc/naming-conventions.md)
- Use ES6+ standard Javascript: [see resource](https://devhints.io/es6)
- Prefer Typescript over Javascript
- Write small, exported (named) functions
- Prefer importing named functions
- Prefer functions before classes unless they are factories or similar more complex patterns (for example, needs to isolate private methods etc.)
- Never use var
- Always prefix (asset) paths with / (beginning slash)
- Always give explicit values (default values) for non-required parameters in JS functions

## Documentation

- Use [JSDoc](http://usejsdoc.org/about-getting-started.html) notation to document non-trivial functions
- Always document state of functions if not final (for example, TODO, UNFINISHED, WIP…)

## Programming

- Doing complex stuff? To avoid poor-performing code, see if there are suggestions [available here](https://github.com/trekhleb/javascript-algorithms/blob/master/README.md)
- Class names with Pascal case (camel case with first letter capitalized), example: `class Player`
- Functions with camel case, example: `function getPlayerCount()`
- Constants with all caps, example: `const NUM_PLAYERS = 2`
- Prefer template literal strings rather than concatenating strings
- Always question the need for a library, especially heavy ones doing ”ordinary” tasks (Bluebird, jQuery, Lodash, Moment…) - Consider alternatives like native JS [Fetch](https://developer.mozilla.org/en/docs/Web/API/Fetch_API), [Day.js](https://github.com/iamkun/dayjs), native JS locales for [dates](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/toLocaleDateString) or [numbers](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/toLocaleString)
- Don’t put polyfills in a regular bundle, use [polyfill.io](https://polyfill.io/v2/docs/) or [babel-polyfill](https://babeljs.io/docs/en/babel-polyfill.html) based on browserslistrc
- Only ever polyfill if really necessary, and see if it's possible to do conditional or smart loading for only those who need polyfilled functionality

## Testing and workflow

- To improve test quality and speed up dev time, write your test and implementation at the same time; Use Jest
- Always be testing (especially IE11); Free virtual machines are [available from Microsoft](https://developer.microsoft.com/en-us/microsoft-edge/tools/vms/); Use [VirtualBox](https://www.virtualbox.org) (free) to run them
- Don’t be clever - make code easy to follow
- Comment where needed
- Don’t repeat yourself

## React

- In React, prefer stateless functional components
- In React, prefer ”function as child” or ”render props” over HOCs and mixins
- In React, always use prop types and default props for non-required props
- Use SUITCSS-style class. A component called ContentBlock should for example use classes such as ContentBlockInside, ContentBlockContainer…
- Do not be overly specific with selectors. Using SUITCSS should allow for very clear, non-nested structures

## Git

- Always write meaningful commit messages
- Use the active tense: ”Add button ’Add to Cart’”, ”Update Webpack config for server-side rendering”
- Follow Gitflow standards as that is the principal method we follow
- Always pull and integrate/merge latest develop branch into your feature branch before sending a pull request
- Code that does not conform to the quality checks of linting or otherwise listed here shall not be pushed to master before fixes have been made
