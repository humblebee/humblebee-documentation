# Infrastructure and Deployment (WIP)

## Overall flow

- Skapa remote; Initial push - vem, hur?
- Bitbucket access etc
- Netlify: länka till repo
- Netlify inställningar
- Cloudflare, skapa CNAMEs
- (Domänhost, namnservrar?)

## Bitbucket

Ha ett kommando för ”setup:initialPush” som gör add, commit, push, set upstream remote
Skapar sedan alla branches: dev, cms, ui, design, test

## Netlify

Snyggaste sättet att göra branch deploys utan att ha massa olika sites i Netlify?

## Cloudflare

Kan automatisera skapandet av zon och DNS-adds för domän och subdomäner
Namnservrar?
