# Humblebee stack and metrics

At Humblebee we most often use our own "boilerplates" for front- and backends. These should support the below overall stack and metrics. In case there are deviations, make sure that we sufficiently address the below concerns and requirements.

## Front-end

- **Renderer**: React 16
- **State management**: Unistore or Redux
- **State persistence**: Native LocalStorage with potential helper package if needed (such as Unissist)

## Build tools

- **Bundler**: Webpack 4
- **Compiler**: Babel 7
- **Linting**: ESLint or TSLint
- **Code standard**: ES2017+ or Typescript 3
- **Components**: Styled Components
- **Testing**: Jest and Puppeteer

## Continuous integration/delivery (CI/CD)

- Git
- Bitbucket
- Preferably a static host with automatic build/deploy engine such as Netlify
  - Otherwise: setup a simple pipeline for example with Bitbucket Pipelines
- **Commit-to-deployed metric goal**: Sub-2 minutes

## Environment (”server”)

### **Progressive Web App (preferred mode)**

- Static deployment, for example on Netlify, Firebase Hosting, Amazon S3
- Use a prerendering service, such as prerender.io (provided free via Netlify)
- Preferentially use react-snap or other snapshotting postbuild-processes

### **Server-side rendering**

- Node 10
- **Frontend package (one of the below):**
  - Humblebee boilerplate (may be influenced by Next.js where needed)
  - Next.js
- **Deployed on one of the below:**
  - ”Serverless” environment via Google (Cloud Functions), Amazon (Lambda Functions) or Microsoft (Azure Functions)
  - Next.js with Express on high quality host such as DigitalOcean or directly on Zeit

## Data

- Regular REST (JSON), consider GraphQL for complex applications/projects
- Headless CMS (if we need content management)
  - Contentful
  - Prismic
  - Custom Humblebee CMS
- Databases:
  - Google Cloud Platform: Firestore (or Firebase) preferred for regular databases
  - Amazon Web Services: Recommendation upcoming
  - Microsoft Azure: Recommendation upcoming

Make sure (if possible, and especially if required by law, for example GDPR) that servers are in Europe, preferentially in Sweden or Scandinavia.

## Security

- Assume a security mindset
  - Only use HTTPS environments
  - Activate SSL/TLS
  - Deny non-secure connections if unable to upgrade requests to HTTPS
  - Use Subresource Integrity on all external resources
- Very strict server response headers
  - Content Security Policy with minimum B-grade compliance (requires minimal external domain resources)
  - Feature policy which is heavily constrained
  - If possible, purge any server signatures or ”x-created-by” (or similar) headers

### Mozilla Observatory goal metrics

- **Level 1**: A or better for ideal circumstances
- **Level 2**: B or better for non-ideal circumstances

### Security Headers goal metrics

- **Level 1**: A+ for ideal circumstances
- **Level 2**: A for non-ideal circumstances

## Performance

- Use a global CDN, load times should be about the same wherever the user is located
- Use route splitting (optionally component splitting)
- Use aggressive Service Worker caching, assume that every hard asset should be cached after first use
- Use ”defer”, ”preload” and ”preconnect” patterns
- Use HSTS preloading
- Use long-term caching via server headers (minimum 1 year if unchanged)

### Google Lighthouse goal metrics

- **Level 1**: 90+ performance overall for ideal circumstances
- **Level 2**: 75+ performance overall for non-ideal circumstance

### Load time metrics (Chrome network panel)

- Note: Remove 1 second FCP time for SSR applications
- **Level 1**: Sub-3 second first contentful paint on ”Fast 3G”, Fully loaded in sub-4 seconds on desktop; Sub-500 kb data transfer; Sub-30 requests
- **Level 2**: Sub-4 second first contentful paint on ”Fast 3G”; Fully loaded in sub-5 seconds on desktop; Sub-800 kb data transfer; Sub-50 requests
